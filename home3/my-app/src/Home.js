
import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route,
  Router,
  Link
} from "react-router-dom";

let HomeArr=[
    {id:0, product:<img src='https://jcement.ru/upload/iblock/79e/058855542-077.jpg'></img>},
    {id:2, product:'ООО «Стройбазторг»  является одним из лидеров рынка Беларуси по продаже товаров для ремонта и строительства. Компания работает на рынке более 20 лет и ежегодно демонстрирует стабильный рост не менее 20%.'}
]

export default function Home() {
    return (
        <div>
            {HomeArr.map((HomeArr)=><div key={HomeArr.id}>
                <div>{HomeArr.product}</div>
            </div>)}
        </div>
    )
}
